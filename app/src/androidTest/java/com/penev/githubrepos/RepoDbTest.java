package com.penev.githubrepos;

import com.penev.githubrepos.db.CashedRepo;
import com.penev.githubrepos.db.RepoDb;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class RepoDbTest {

	private static final int ID = 999;

	private static final String NAME = "test_repo";

	private static final String DESCRIPTION = "test_description";

	private static final String OWNER_LOGIN = "test_owner_login";

	private static final String OWNER_URL = "test_owner_url";

	private static final String REPO_URL = "test_repo_url";

	private static final boolean FORK = false;

	@Test
	public void testRepoDb() {
		CashedRepo cashedRepo = new CashedRepo();
		cashedRepo.setId(ID);
		cashedRepo.setName(NAME);
		cashedRepo.setDescription(DESCRIPTION);
		cashedRepo.setOwnerLogin(OWNER_LOGIN);
		cashedRepo.setOwnerUrl(OWNER_URL);
		cashedRepo.setRepoUrl(REPO_URL);
		cashedRepo.setFork(FORK);

		RepoDb.getInstance().cashedRepoDao().insertAll(cashedRepo);

		CashedRepo savedRepo = RepoDb.getInstance().cashedRepoDao().getRepoWithId(ID);

		Assert.assertNotNull(savedRepo);
		Assert.assertTrue(savedRepo.getName().equals(NAME));
		Assert.assertTrue(savedRepo.getDescription().equals(DESCRIPTION));
		Assert.assertTrue(savedRepo.getOwnerLogin().equals(OWNER_LOGIN));
		Assert.assertTrue(savedRepo.getOwnerUrl().equals(OWNER_URL));
		Assert.assertTrue(savedRepo.getRepoUrl().equals(REPO_URL));
		Assert.assertFalse(savedRepo.isFork());

		long result = RepoDb.getInstance().cashedRepoDao().deleteRepoWithId(ID);

		Assert.assertTrue(result == 1);

		CashedRepo deletedRepo = RepoDb.getInstance().cashedRepoDao().getRepoWithId(ID);
		Assert.assertNull(deletedRepo);
	}
}
