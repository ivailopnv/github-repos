package com.penev.githubrepos;

import android.support.test.rule.ActivityTestRule;

import com.penev.githubrepos.activity.MainListActivity;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isEnabled;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(JUnit4.class)
public class MainListActivityTest {

	@Rule
	public ActivityTestRule<MainListActivity> mActivityRule =
			new ActivityTestRule<>(MainListActivity.class);

	@Test
	public void testMainActivity() throws Exception {
		Assert.assertNotNull(mActivityRule);

		onView(withId(R.id.github_repos_recycler_view))
				.check(matches(isDisplayed()))
				.check(matches(isEnabled()));
	}
}
