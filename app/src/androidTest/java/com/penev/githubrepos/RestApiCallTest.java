package com.penev.githubrepos;

import android.arch.paging.PagedListAdapter;

import com.penev.githubrepos.pojo.Repo;
import com.penev.githubrepos.retrofit.RetrofitUtils;
import com.penev.githubrepos.utils.Utils;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;
import java.util.Random;

@RunWith(JUnit4.class)
public class RestApiCallTest {

	@Test
	public void testGitHubApi() throws Exception {

		if (Utils.isDeviceConnected()) {
			List<Repo> response = RetrofitUtils.loadGitHubRepos(1);
			Repo randomRepo = response.get(new Random().nextInt(9));

			Assert.assertNotNull(response);
			Assert.assertTrue(response.size() == 10);

			for (Repo repo:response) {
				Assert.assertTrue(repo.getId() != 0);
				Assert.assertTrue(repo.getName() != null && !repo.getName().isEmpty());
				Assert.assertTrue(repo.getDescription() != null &&
						!repo.getDescription().isEmpty());
				Assert.assertTrue(repo.getOwner().getLogin() != null &&
						!repo.getOwner().getLogin().isEmpty());
				Assert.assertTrue(repo.getUrl() != null &&
						!repo.getUrl().isEmpty());
				Assert.assertTrue(repo.getOwner().getUrl() != null &&
						!repo.getOwner().getUrl().isEmpty());
			}

			List<Repo> response2 = RetrofitUtils.loadGitHubRepos(2);

			for (Repo repo : response2) {
				Assert.assertFalse(randomRepo.getId().equals(repo.getId()));
			}
		}
	}
}
