package com.penev.githubrepos.adapter;


import android.arch.paging.PagedListAdapter;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.penev.githubrepos.R;
import com.penev.githubrepos.pojo.Repo;
import com.penev.githubrepos.view.RepoViewHolder;

public class ReposListAdapter extends PagedListAdapter<Repo, RepoViewHolder> {

	private Context mContext;

	public ReposListAdapter(Context context) {
		super(Repo.DIFF_CALLBACK);
		mContext = context;
	}

	@Override
	public RepoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_repo_item,
				parent, false);
		return new RepoViewHolder(mContext, view);
	}

	@Override
	public void onBindViewHolder(RepoViewHolder holder, int position) {
		holder.bindTo(getItem(position));
	}
}