package com.penev.githubrepos.retrofit;


import com.penev.githubrepos.pojo.Repo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;

public class RetrofitUtils {

	private static final String REPOS_PER_PAGE = "10";

	public static final String ACCESS_TOKEN = "879eee81e64f87a925dc7ada76eea9f63f492417";

	public static List<Repo> loadGitHubRepos(int page) throws Exception {
		RetrofitService retrofitService = RetrofitGenerator.createService();
		Map<String, String> options = new HashMap<>();
		options.put("page", Integer.toString(page));
		options.put("per_page", REPOS_PER_PAGE);
		options.put("access_token", ACCESS_TOKEN);
		Call<List<Repo>> call = retrofitService.getGitHubRepos(options);
		return call.execute().body();
	}
}
