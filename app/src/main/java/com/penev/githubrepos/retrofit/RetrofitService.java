package com.penev.githubrepos.retrofit;


import com.penev.githubrepos.pojo.Repo;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface RetrofitService {

	@GET("repos")
	Call<List<Repo>> getGitHubRepos(@QueryMap Map<String, String> options);
}
