package com.penev.githubrepos.retrofit;


import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitGenerator {

	private static final String BASE_URL = "https://api.github.com/users/xing/";

	private static Retrofit retrofit;

	private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

	private static Retrofit.Builder builder = new Retrofit.Builder()
			.addConverterFactory(GsonConverterFactory.create())
			.client(httpClient.build());

	public static RetrofitService createService() {
		if (retrofit == null) {
			retrofit = builder
					.baseUrl(BASE_URL)
					.build();
		}
		return retrofit.create(RetrofitService.class);
	}
}
