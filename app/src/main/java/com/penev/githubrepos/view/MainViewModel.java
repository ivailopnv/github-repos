package com.penev.githubrepos.view;


import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;

import com.penev.githubrepos.data.ReposDataSourceFactory;
import com.penev.githubrepos.pojo.Repo;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class MainViewModel extends ViewModel {

	public LiveData<PagedList<Repo>> reposList;

	public MainViewModel() {
		Executor executor = Executors.newFixedThreadPool(5);
		ReposDataSourceFactory factory = new ReposDataSourceFactory();

		PagedList.Config pagedListConfig = (new PagedList.Config.Builder()).setEnablePlaceholders(false)
						.setInitialLoadSizeHint(10)
						.setPageSize(10)
						.build();

		reposList = (new LivePagedListBuilder(factory, pagedListConfig))
							.setBackgroundThreadExecutor(executor)
							.build();
	}
}
