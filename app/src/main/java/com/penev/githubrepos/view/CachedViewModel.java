package com.penev.githubrepos.view;


import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;

import com.penev.githubrepos.db.CashedRepo;
import com.penev.githubrepos.db.RepoDb;

public class CachedViewModel extends ViewModel {

	public LiveData<PagedList<CashedRepo>> liveData;

	public CachedViewModel() {
		RepoDb repoDb = RepoDb.getInstance();
		liveData = new LivePagedListBuilder<>(repoDb.cashedRepoDao().getRepos(),20).build();
	}
}
