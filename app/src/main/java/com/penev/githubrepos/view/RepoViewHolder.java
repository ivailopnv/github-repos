package com.penev.githubrepos.view;


import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.penev.githubrepos.R;
import com.penev.githubrepos.db.CashedRepo;
import com.penev.githubrepos.fragment.ChooseUrlDialog;
import com.penev.githubrepos.pojo.Repo;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnLongClick;

public class RepoViewHolder extends RecyclerView.ViewHolder {

	@BindView(R.id.repo_name_text_view) TextView mRepoNameTextView;
	@BindView(R.id.repo_description_text_view) TextView mRepoDescriptionTextView;
	@BindView(R.id.repo_owner_login_text_view) TextView mRepoOwnerLoginTextView;

	@BindView(R.id.decor_view)View mDecorView;

	private Context mContext;

	private String mRepoUrl;

	private String mOwnerUrl;

	public RepoViewHolder(Context context, View itemView) {
		super(itemView);
		ButterKnife.bind(this, itemView);
		mContext = context;
	}

	public void bindTo(Repo repo) {
		mRepoUrl = repo.getUrl();
		mOwnerUrl = repo.getOwner().getUrl();
		mRepoNameTextView.setText(repo.getName());
		mRepoDescriptionTextView.setText(repo.getDescription());
		mRepoOwnerLoginTextView.setText(repo.getOwner().getLogin());

		if (repo.getFork() == null || !repo.getFork()) {
			mDecorView.setBackgroundResource(R.color.colorLightGreet);
		} else {
			mDecorView.setBackgroundResource(R.color.colorWhite);
		}
	}

	public void bindFromCache(CashedRepo repo) {
		mRepoUrl = repo.getRepoUrl();
		mOwnerUrl = repo.getOwnerUrl();
		mRepoNameTextView.setText(repo.getName());
		mRepoDescriptionTextView.setText(repo.getDescription());
		mRepoOwnerLoginTextView.setText(repo.getOwnerLogin());

		if (!repo.isFork()) {
			mDecorView.setBackgroundResource(R.color.colorLightGreet);
		} else {
			mDecorView.setBackgroundResource(R.color.colorWhite);
		}
	}

	@OnLongClick(R.id.decor_view)
	public boolean onDecorLongClick(View v) {
		FragmentManager fm = ((AppCompatActivity)mContext).getSupportFragmentManager();
		ChooseUrlDialog dialog = ChooseUrlDialog.newInstance(mRepoUrl, mOwnerUrl);
		dialog.show(fm, ChooseUrlDialog.TAG);
		return true;
	}
}
