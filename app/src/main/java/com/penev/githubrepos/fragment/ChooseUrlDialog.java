package com.penev.githubrepos.fragment;


import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.penev.githubrepos.R;


public class ChooseUrlDialog extends DialogFragment {

	public static final String TAG = "choose_url_fragment";

	public static final String KEY_REPO_URL = "repo_url";

	public static final String KEY_OWNER_URL = "owner_url";

	public static ChooseUrlDialog newInstance(String repoUrl, String ownerUrl) {
		ChooseUrlDialog frag = new ChooseUrlDialog();
		Bundle args = new Bundle();
		args.putString(KEY_REPO_URL, repoUrl);
		args.putString(KEY_OWNER_URL, ownerUrl);
		frag.setArguments(args);
		return frag;
	}

	@NonNull
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		if (getActivity() == null) return super.onCreateDialog(savedInstanceState);
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
		alertDialogBuilder.setTitle(getString(R.string.url_dialog_title));
		alertDialogBuilder.setMessage(getString(R.string.url_dialog_msg));
		alertDialogBuilder.setPositiveButton(getString(R.string.url_dialog_repo_button),
				(dialogInterface, i)
				-> openUrl(getArguments().getString(KEY_REPO_URL)));

		alertDialogBuilder.setNegativeButton(getString(R.string.url_dialog_owner_button),
				(dialogInterface, i)
				-> openUrl(getArguments().getString(KEY_OWNER_URL)));

		return alertDialogBuilder.create();
	}

	private void openUrl(String url) {
		Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		try {
			startActivity(browserIntent);
		} catch (ActivityNotFoundException e) {
			Log.e(TAG, e.toString());
		}
	}
}
