package com.penev.githubrepos.utils;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import com.penev.githubrepos.MyApplication;

public class Utils {

	public static final String TAG = "GitHReposTag";

	public static boolean isDeviceConnected() {
		try {
			ConnectivityManager cm = (ConnectivityManager) MyApplication.getAppContext()
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
			return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
		} catch (Exception e) {
			Log.e(Utils.TAG, e.getMessage(), e);
			return false;
		}
	}

	public static void showToast(Context context, int stringId) {
		Toast.makeText(context, stringId, Toast.LENGTH_SHORT).show();
	}
}
