package com.penev.githubrepos.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.DiffCallback;

@Entity (tableName = CashedRepo.TABLE_NAME, indices = {@Index(value = CashedRepo.COLUMN_ID, unique = true)})
public class CashedRepo {

	public static final String TABLE_NAME = "repos";

	public static final String COLUMN_ID = "repo_id";

	public static final String COLUMN_NAME = "name";

	public static final String COLUMN_DESCRIPTION = "description";

	public static final String COLUMN_OWNER_LOGIN = "owner_login";

	public static final String COLUMN_REPO_URL = "repo_url";

	public static final String COLUMN_OWNER_URL = "owner_url";

	public static final String COLUMN_FORK_FLAG = "fork";

	@PrimaryKey(autoGenerate = true)
	private int uid;

	@ColumnInfo(name = COLUMN_ID)
	private int id;

	@ColumnInfo(name = COLUMN_NAME)
	private String name;

	@ColumnInfo(name = COLUMN_DESCRIPTION)
	private String description;

	@ColumnInfo(name = COLUMN_OWNER_LOGIN)
	private String ownerLogin;

	@ColumnInfo(name = COLUMN_REPO_URL)
	private String repoUrl;

	@ColumnInfo(name = COLUMN_OWNER_URL)
	private String ownerUrl;

	@ColumnInfo(name = COLUMN_FORK_FLAG)
	private boolean fork;

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getOwnerLogin() {
		return ownerLogin;
	}

	public void setOwnerLogin(String ownerLogin) {
		this.ownerLogin = ownerLogin;
	}

	public String getRepoUrl() {
		return repoUrl;
	}

	public void setRepoUrl(String repoUrl) {
		this.repoUrl = repoUrl;
	}

	public String getOwnerUrl() {
		return ownerUrl;
	}

	public void setOwnerUrl(String ownerUrl) {
		this.ownerUrl = ownerUrl;
	}

	public boolean isFork() {
		return fork;
	}

	public void setFork(boolean fork) {
		this.fork = fork;
	}

	public static final DiffCallback<CashedRepo> DIFF_CALLBACK = new DiffCallback<CashedRepo>() {
		@Override
		public boolean areItemsTheSame(@NonNull CashedRepo oldRepo, @NonNull CashedRepo newRepo) {
			return oldRepo.getId() == newRepo.getId();
		}

		@Override
		public boolean areContentsTheSame(@NonNull CashedRepo oldRepo, @NonNull CashedRepo newRepo) {
			return oldRepo.getName().equals(newRepo.getName());
		}
	};
}
