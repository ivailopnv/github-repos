package com.penev.githubrepos.db;

import android.arch.paging.DataSource;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

@Dao
public interface CashedRepoDao {

	@Insert
	void insertAll(CashedRepo... repo);

	@Query("SELECT * FROM " + CashedRepo.TABLE_NAME)
	DataSource.Factory<Integer, CashedRepo> getRepos();

	@Query("SELECT * FROM " + CashedRepo.TABLE_NAME + " WHERE " + CashedRepo.COLUMN_ID + " LIKE :repoId")
	CashedRepo getRepoWithId(int repoId);

	@Query("DELETE FROM " + CashedRepo.TABLE_NAME + " WHERE " + CashedRepo.COLUMN_ID + " LIKE :repoId")
	int deleteRepoWithId(int repoId);
}
