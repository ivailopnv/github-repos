package com.penev.githubrepos.db;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;

import com.penev.githubrepos.MyApplication;

@Database(version = 1, entities = {CashedRepo.class})
public abstract class RepoDb extends RoomDatabase {

	public static final String DATABASE_NAME = "repos_database";

	private static RepoDb instance;

	public static synchronized RepoDb getInstance() {
		if (instance == null) {
			instance = Room.databaseBuilder(MyApplication.getAppContext().getApplicationContext(),
					RepoDb.class, DATABASE_NAME).build();
		}
		return instance;
	}

	public abstract CashedRepoDao cashedRepoDao();
}
