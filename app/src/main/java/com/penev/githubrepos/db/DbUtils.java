package com.penev.githubrepos.db;

import com.penev.githubrepos.pojo.Repo;

import java.util.List;

public class DbUtils {

	public static void cashFromNetwork(List<Repo> repos) {
		RepoDb db = RepoDb.getInstance();

		for (Repo repo : repos) {
			CashedRepo cashedRepo = new CashedRepo();
			cashedRepo.setId(repo.getId());
			cashedRepo.setName(repo.getName());
			cashedRepo.setDescription(repo.getDescription());
			cashedRepo.setOwnerLogin(repo.getOwner().getLogin());
			cashedRepo.setOwnerUrl(repo.getOwner().getUrl());
			cashedRepo.setRepoUrl(repo.getUrl());

			boolean fork = !(repo.getFork() == null || !repo.getFork());
			cashedRepo.setFork(fork);

			db.cashedRepoDao().insertAll(cashedRepo);
		}
	}
}
