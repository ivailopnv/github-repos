package com.penev.githubrepos.data;


import android.arch.paging.PageKeyedDataSource;
import android.support.annotation.NonNull;
import android.util.Log;

import com.penev.githubrepos.db.DbUtils;
import com.penev.githubrepos.retrofit.RetrofitUtils;
import com.penev.githubrepos.pojo.Repo;

import java.util.List;

import static com.penev.githubrepos.utils.Utils.TAG;

public class PageKeyReposDataSource extends PageKeyedDataSource<Integer, Repo> {

	@Override
	public void loadInitial(@NonNull LoadInitialParams<Integer> params,
							@NonNull LoadInitialCallback<Integer, Repo> callback) {
		try {
			List<Repo> repos = RetrofitUtils.loadGitHubRepos(1);

			if (repos != null) {
				callback.onResult(repos, 0, 1000, null, 2 );
				DbUtils.cashFromNetwork(repos);
			}
		} catch (Exception e) {
			Log.e(TAG, e.toString());
		}
	}

	@Override
	public void loadAfter(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer,
			Repo> callback) {
		try {
			List<Repo> repos = RetrofitUtils.loadGitHubRepos(params.key);

			if (repos != null) {
				callback.onResult(repos, params.key + 1);
				DbUtils.cashFromNetwork(repos);
			}
		} catch (Exception e) {
			Log.e(TAG, e.toString());
		}
	}

	@Override
	public void loadBefore(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer,
			Repo> callback) {}
}
