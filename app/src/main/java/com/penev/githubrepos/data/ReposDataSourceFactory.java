package com.penev.githubrepos.data;


import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.DataSource;

public class ReposDataSourceFactory implements DataSource.Factory {

	private MutableLiveData<PageKeyReposDataSource> mMutableLiveData;

	public ReposDataSourceFactory() {
		mMutableLiveData = new MutableLiveData<>();
	}

	@Override
	public DataSource create() {
		PageKeyReposDataSource dataSource = new PageKeyReposDataSource();
		mMutableLiveData.postValue(dataSource);
		return dataSource;
	}
}
