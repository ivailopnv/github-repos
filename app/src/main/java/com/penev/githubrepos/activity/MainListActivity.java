package com.penev.githubrepos.activity;

import android.arch.lifecycle.ViewModelProviders;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.penev.githubrepos.R;
import com.penev.githubrepos.adapter.CashedReposListAdapter;
import com.penev.githubrepos.adapter.ReposListAdapter;
import com.penev.githubrepos.view.CachedViewModel;
import com.penev.githubrepos.view.MainViewModel;
import com.penev.githubrepos.view.MainViewModelFactory;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.penev.githubrepos.utils.Utils.isDeviceConnected;
import static com.penev.githubrepos.utils.Utils.showToast;

public class MainListActivity extends AppCompatActivity {

	@BindView(R.id.github_repos_recycler_view) RecyclerView mReposRecycleView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_list);
		ButterKnife.bind(this);
		LinearLayoutManager lm = new LinearLayoutManager(this,
				LinearLayoutManager.VERTICAL, false);
		mReposRecycleView.setLayoutManager(lm);

		if (isDeviceConnected()) {
			loadFromNetwork();
		} else {
			loadFromCache();
		}
	}

	private void loadFromNetwork() {
		MainViewModel viewModel = ViewModelProviders.of(this,
				new MainViewModelFactory()).get(MainViewModel.class);
		final ReposListAdapter adapter = new ReposListAdapter(this);
		viewModel.reposList.observe(this, adapter::setList);
		mReposRecycleView.setAdapter(adapter);
		showToast(this, R.string.mst_load_from_network);
	}

	private void loadFromCache() {
		CachedViewModel viewModel = ViewModelProviders.of(this).get(CachedViewModel.class);
		final CashedReposListAdapter adapter = new CashedReposListAdapter(this);
		viewModel.liveData.observe(this, adapter::setList);
		mReposRecycleView.setAdapter(adapter);
		showToast(this, R.string.mst_load_from_cache);
	}
}
