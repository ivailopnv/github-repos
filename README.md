# Project documentation
## Introduction
The application starts with a main screen showing a list view of the repositories. If the devices is connected to a network (WIFI or Cellular) the app will start fetching items from the REST API (github) otherwise it will load from the cash (local sqlite database) if there are any previously saved. In the case of a network fetch, only 10 items will be requested at a time so as the user keep scrolling down new requests will be made.

When the user does a long press click on an item of the list a new dialog will show. From there he can chose to visit the repository url or the owner url.

A short video demo of the app can be found [here.](https://drive.google.com/file/d/1cApwJKKSbWZk4TMtYk-5qfp9FKZk4JBQ/view?usp=sharing)

The source code of this project tries to follow the Android and Java guidelines described [here.](https://bitbucket.org/ivailopnv/android-guildlines/src/4a757ee3b90ad624b99e8cb446c2386360b391b4/android_guildlines.md?at=master&fileviewer=file-view-default)
## Architecture
The application is using Google's architecture components libraries and the MVC pattern in order to achieve paging. One of the biggest advantages of this library is the lifecycle awareness. The data providers will be aware of the lifecycle state of the UI components responsible for displaying the data (activity or fragment). Another nice feature is the integration will **Room**, recently released library by Google (also part from the architecture components). The library is taking care of the local SQlite interactions.

The flow stars from the `PageKeyRepossDataSource`. The class uses Retrofit calls to pull pages of data from the REST APIs. The initial call will define the maximum number of results that can be fetched from the server. After the initial call the `laodAfter()` callback will continue to fetch by incrementing the page number. After every call in `loadInitial()` and `laodAfter()` the fetched items will be stored in a local DB using `Room`. The repository int id is selected as a unique entry in the DB to avoid duplication.

The `PagedList` API is then responsible for loading data from the data source object. There we can configure how much data is loaded at a time and how much should be prefetch. This is implemented inside the `MainViewModel` class. The data loading is done in a background executor thread.

`PageListAdapter` is taking care of presenting the data from  a `PagedList`. The adapter is used with a standard `RecyclerView`. 
## 3rd Party libraries 
### Android support ###
[Source](https://developer.android.com/topic/libraries/support-library/packages.html)

This includes the v7 version and mainly its ui modules such as: **appcompat, constrain-layout, recyclerview** . Also the support version and its modules: **runer and espresso**

### Android Arch ###
[Source](https://developer.android.com/topic/libraries/architecture/adding-components.html)

This package included google's architecture components modules such as: **lifecycle, paging and room**

### Retrofit 2 ###
[Source](http://square.github.io/retrofit/)

Popular library by Square for executing HTTP calls. The library is mainly used for calls to the Movie Database API's.

### ButterKnife ###
[Source](http://jakewharton.github.io/butterknife/)

Another popular library by the Jake Wharton. Used to bind views into Activities and Fragments using annotations. 

### Debug DB ###
[Source](https://github.com/amitshekhariitbhu/Android-Debug-Database)

Databse library thay I like to use for debuging purposees. It allows to display and iteract with the app's databse in a web browser. The librarie is only included in debug build for security reasons.
